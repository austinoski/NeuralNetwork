from random import random, seed
from math import exp




class Neuron(object):
    def __init__(self, weights):
        self.output = None
        self.error = None
        self.weights = weights

    def activate(self, input_, bias):
        activation = bias * self.weights[-1]
        for index, weights in enumerate(self.weights[:-1]):
            activation += (weights * input_[index])
        return activation

    def __str__(self):
        return "output: {}, error: {}".format(self.output, self.error)




class Network(object):
    def __init__(self, n_input=2, n_hidden=8, n_output=1):
        self.n_input = n_input
        self.n_output = n_output
        self.n_hidden = n_hidden
        self.bias = 0.0
        self.network = []
        self.init_network()

    def init_network(self):
        hidden = [Neuron([random() for r in range(self.n_input+1)]) for l in range(self.n_hidden)]
        output = [Neuron([random() for r in range(self.n_hidden+1)]) for l in range(self.n_output)]
        self.network.append(hidden)
        self.network.append(output)

    def feed_forward(self, input_):
        inputs = [input_]
        for index, layer in enumerate(self.network):
            temp = [] # holding output of current layer to use as input for next
            for neuron in layer:
                activation = neuron.activate(inputs[index], self.bias)
                neuron.output = self.sigmoid(activation)
                temp.append(neuron.output)
            inputs.append(temp)

    def backpropagate(self, expected):
        length = len(self.network)
        for index in range(length-1, -1, -1):
            if index == length - 1:
                for index2, neuron in enumerate(self.network[index]):
                    error = expected[index2] - neuron.output
                    neuron.error = error * self.sigmoid_deriv(neuron.output)
            else:
                for index2, neuron in enumerate(self.network[index]):
                    error = 0.0
                    for neuron2 in self.network[index+1]:
                        error += (neuron2.weights[index2] * neuron2.error)
                    neuron.error = error * self.sigmoid_deriv(neuron.output)

    def train(self, inputs, expected, learning=0.5, epoch=100000, bias=0.0):
        self.bias = bias
        for i in range(epoch):
            for j in range(len(inputs)):
                self.feed_forward(inputs[j])
                self.backpropagate(expected[j])
                self.update_weights(inputs[j], learning)
            error = 0.0
            for neuron in self.network[-1]:
                error += neuron.error
            self.total_error = error
            net.display("error")

    def update_weights(self, input_, learning):
        inputs = [input_ + [self.bias]]
        for index, layer in enumerate(self.network):
            temp = []
            for neuron in layer:
                for i in range(len(inputs[index])):
                    neuron.weights[i] += (learning * neuron.error * inputs[index][i])
                temp.append(neuron.output)
            inputs.append(temp + [self.bias])

    def sigmoid(self, activation):
        return 1 / (1 + exp(-activation))

    def sigmoid_deriv(self, output):
        return output * (1 - output)

    def display(self, mode=None):
        for index, layer in enumerate(self.network):
            print(index)
            for neuron in layer:
                if mode is "weights":
                    print(neuron.weights)
                elif mode is "error":
                    print(self.total_error)
                else:
                    print(neuron)





if __name__ == '__main__':
    seed(1)
    net = Network(2, 10, 1)
    inputs = [[0,0], [1,0], [0,1], [1,1]]
    expected = [[1], [0], [0], [1]]
    net.train(inputs, expected, epoch=1000)
    while True:
        a = int(input("--> "))
        b = int(input("--> "))
        net.feed_forward([a,b])
        net.display()


